// New 23.02.22 Old 20.02.22
// EMUS - SMA
//////////////////////////////////////////
//#include <CAN.h>
#include "ESP32SJA1000.h"
ESP32SJA1000Class CANESP;
#define ON  LOW
#define OFF HIGH
#include <EEPROM.h>
#include "SD.h"
//////////////////////////////////////////
#define SD_MOSI      18
#define SD_MISO      19
#define SD_SCK       5
#define SD_CS_PIN   14
//////////////////////////////////////////
bool ActiveSDcard = true;
File myFile;
SPIClass SPISD(HSPI);
String semi = ",";
bool checkonce = true;
int counterdatamissing = 0;
int LED_R = 2;
int LED_B = 4;
int LED_G = 15;

const unsigned long eventIntervalCANbus = 1000;
const unsigned long eventIntervalSerial = 1000;
const unsigned long eventIntervalCommu = 1000;

unsigned long previousTime1 = 0, previousTime2 = 0, previousTime3 = 0, previousTime4 = 0, previousTime5 = 0;

int Errors_Master = 0, Warnings_Master = 0;

float CounterTime, BattCharVolt, ChargCurrLim, DischCurrLim,  DischVolt, SOC, SOH, HiSOC, BattVoltage, BatterCurrt, BattTempe, BatteryAlarams0, BatteryAlarams1, BatteryAlarams2, BatteryAlarams3, BatteryWanings;
float SMABattVolt, SMABattCurrent , SMABattTemp, SMASOC, SMAChar_OpeStae, SMAErrorMEss, SMABattCharPoint;

byte binaryNum[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

byte WarndataMaster[8];
byte ErrordataMaster1[8];
byte ErrordataMaster6[8];
byte WarndataMaster5[8];
byte WarndataMaster4[8];
byte ErrordataMaster2[8];
int Errors_Master1, Errors_Master2, Errors_Master6;

byte errorbit0, errorbit1, errorbit2, errorbit3;
byte warnbit4, warnbit5, warnbit6, warnbit7;

int Reset_HighTemp_Current = 0, Reset_WHighTemp_Current = 0, Reset_WLowTemp_Current = 0, Reset_LowTemp_Current = 0;
int MasterCANbus = 0, MasterCANError = 1;
const float scalingfact1 = 0.1;
const float scalingfact2 = 0.01;
String Batt = "/EMUS_Battery";
String EpromN = "0";
String Csv = ".csv";
String Filename;
int positionEEprom = 3;
//////////////////////////
byte data355[8], data356[8], data351[8], data189[8], data305[8], data306[8];
/////////////////////////////////////
void setup() {
  delay(3000);
  Serial.begin(9600);
  //////////////////////////////////////////
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
  digitalWrite(LED_B, OFF);
  digitalWrite(LED_G, OFF);
  digitalWrite(LED_R, OFF);
  /////////////////////////////////////////
  if (ActiveSDcard) {
    pinMode(SD_CS_PIN, OUTPUT);
    pinMode(SD_CS_PIN, INPUT_PULLUP);
    digitalWrite(SD_CS_PIN, HIGH);
    //////////////////////////////////
    SPISD.begin(SD_SCK, SD_MISO, SD_MOSI);
    if (!SD.begin(SD_CS_PIN, SPISD))
    {
      Serial.println(F("Card failed, or not present!"));
      digitalWrite(LED_B, ON);
      digitalWrite(LED_G, OFF);
      digitalWrite(LED_R, OFF);
      return;
    }
    else
    {
      Serial.println(F("SD is ready to Write!"));
    }
  }
  //////////////////////////////////////
  // register the receive callback
  CANESP.setPins(26, 25); /// CANbus
  CANESP.begin(500E3);    /// 250 kbits
  CANESP.onReceive(onReceive);
  delay(3000);
  while (!Serial);
  //////////////////////////////////
  if (ActiveSDcard) {
    EEPROM.begin(positionEEprom + 2); /// increment more than read bytes position
    EEPROM.write(positionEEprom, EEPROM.read(positionEEprom) + 1);
    EEPROM.commit();
    int check1 =  EEPROM.read(positionEEprom);
    EpromN = String(check1);
    Filename =  Batt + EpromN + Csv;
    Serial.println(Filename);
    //////////////////////////////////
    myFile = SD.open(Filename, "a"); //append to file
    String semi = ",";
    String Headline = "1s Counter" + semi + "Battcharvolt"  + semi + "CharCurrLimi"  + semi + "DischarCurrLimi"  + semi + "Battdischarvolt"  + semi + "SOC"
                      + semi + "SOH"  + semi + "ResSOC"  + semi + "BatteryVoltage"  + semi + "BatteryCurrent"  + semi +  "BatteryTemperature"
                      + semi +   "BatteryAlarams0" + semi +   "BatteryAlarams1" + semi +   "BatteryAlarams2" + semi +   "BatteryAlarams3"
                      + semi + "BatteryWanings" + semi + "SMABattVolt"  + semi + "SMABattCurrent"  + semi +  "SMABattTemp"   + semi + "SMASOC"   + semi + "SMAChar_OpeStae"
                      + semi + "SMAErrorMEss"  + semi + "SMABattCharPoint";
    myFile.println(Headline);
    myFile.close();
    Serial.println("Headlines");
  }
}
/////////////////////////////////////////
void loop() {
  ////////////////////////////////////////////////
  if (CounterTime > 80000) /// 80000 Data points for almost 5 Days, Creates a new Excel
  {
    CounterTime = 0;
    EEPROM.write(positionEEprom, EEPROM.read(positionEEprom) + 1);
    EEPROM.commit();
    int check1 =  EEPROM.read(positionEEprom);
    EpromN = String(check1);
    Filename =  Batt + EpromN + Csv;
    Serial.println(Filename);
    //////////////////////////////////
    myFile = SD.open(Filename, "a"); //append to file
    String semi = ",";
    String Headline = "1s Counter" + semi + "Battcharvolt"  + semi + "CharCurrLimi"  + semi + "DischarCurrLimi"  + semi + "Battdischarvolt"  + semi + "SOC"
                      + semi + "SOH"  + semi + "ResSOC"  + semi + "BatteryVoltage"  + semi + "BatteryCurrent"  + semi +  "BatteryTemperature"
                      + semi +   "BatteryAlarams0" + semi +   "BatteryAlarams1" + semi +   "BatteryAlarams2" + semi +   "BatteryAlarams3"
                      + semi + "BatteryWanings" + semi + "SMABattVolt"  + semi + "SMABattCurrent"  + semi +  "SMABattTemp"   + semi + "SMASOC"   + semi + "SMAChar_OpeStae"
                      + semi + "SMAErrorMEss"  + semi + "SMABattCharPoint";
    myFile.println(Headline);
    myFile.close();
    Serial.println("Headlines");
  }
  ////////////////////////////////////////////////
  unsigned long currentTime = millis();
  if (currentTime - previousTime1 >= eventIntervalSerial) {
    //SerialDebug();
    previousTime1 = currentTime;
  }
  if (currentTime - previousTime2 >= eventIntervalCANbus) {
    RelayControl();
    previousTime2 = currentTime;
  }
  if (currentTime - previousTime5 >= eventIntervalCommu) {
    CANComm();
    Write_SDCard();
    previousTime5 = currentTime;
  }
  ////////////////////////////////
  /// Real Calculation
  BattCharVolt = (float)(data351[1] << 8 | data351[0]) * scalingfact1;
  ChargCurrLim = (float)(data351[3] << 8 | data351[2]) * scalingfact1;
  DischCurrLim = (float)(data351[5] << 8 | data351[4]) * scalingfact1;
  DischVolt = (float)(data351[7] << 8 | data351[6]) * scalingfact1;

  SOC = (float)(data355[1] << 8 | data355[0]);
  SOH = (float)(data355[3] << 8 | data355[2]);
  HiSOC = (float)(data355[5] << 8 | data355[4]) * scalingfact2;

  BattVoltage = (float)(data356[1] << 8 | data356[0]) * scalingfact2;
  String MSB1 = String(data356[3], HEX);
  String LSB1 = String(data356[2], HEX);
  if ((data356[3]) == 0xFF)  {
    if (data356[2] <= 0xF)  {
      LSB1 = "0" + LSB1;
    }
  }
  BatterCurrt = (int16_t)(strtol((MSB1 + LSB1).c_str(), NULL, 16));

  BattTempe = (float)(data356[5] << 8 | data356[4]); /// TEST
  if (BattTempe > 32767) BattTempe = BattTempe - 65536;
  BattTempe = BattTempe * scalingfact1;

  SMABattVolt = (float)(data305[1] << 8 | data305[0]) * scalingfact1;

  String MSB2 = String(data305[3], HEX);
  String LSB2 = String(data305[2], HEX);
  if ((data305[3]) == 0xFF)  {
    if (data305[2] <= 0xF)  {
      LSB2 = "0" + LSB2;
    }
  }
  signed int SMABattCurrent = (int16_t)(strtol((MSB2 + LSB2).c_str(), NULL, 16));

  SMABattTemp = (float)(data305[5] << 8 | data305[4]);
  if (SMABattTemp > 32767) SMABattTemp = SMABattTemp - 65536;
  SMABattTemp = SMABattTemp * scalingfact1;

  SMASOC = (float)(data305[7] << 8 | data305[6]) * scalingfact1;

  SMAChar_OpeStae = (float)(data306[3] << 8 | data306[2]);
  SMAErrorMEss = (float)(data306[5] << 8 | data306[4]);
  SMABattCharPoint = (float)(data306[7] << 8 | data306[6]) * scalingfact1;
  ///----------------------------------------------------
  ///////////////////////////////////////
  Errors_Master1 = data189[0];
  Errors_Master2 = data189[2];
  decToBinary(Errors_Master1);
  for (int j = 7; j >= 0; j--)   {
    ErrordataMaster1[7 - j] = binaryNum[j];
  }
  decToBinary(Errors_Master2);
  for (int j = 7; j >= 0; j--)  {
    ErrordataMaster2[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    ErrordataMaster1[i] = ErrordataMaster1[i];
  }
  for (int i = 0; i <= 7; i++)  {
    ErrordataMaster2[i] = ErrordataMaster2[i];
  }
  ///------------------------------------------------------
  Errors_Master6 = data189[6];
  decToBinary(data189[6]);
  for (int j = 7; j >= 0; j--)  {
    ErrordataMaster6[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    ErrordataMaster6[i] = ErrordataMaster6[i];
  }
  ///------------------------------------------------------
  decToBinary(data189[5]);
  for (int j = 7; j >= 0; j--)  {
    WarndataMaster5[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    WarndataMaster5[i] = WarndataMaster5[i];
  }
  ///------------------------------------------------------
  decToBinary(data189[4]);
  for (int j = 7; j >= 0; j--)  {
    WarndataMaster4[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    WarndataMaster4[i] = WarndataMaster4[i];
  }
  ///----------------------------------------------------
  errorbit0 = 0b10101010;
  errorbit1 = 0b10101010;
  errorbit2 = 0b10101010;
  errorbit3 = 0b10101010;
  BatteryAlarams0 = 0;
  BatteryAlarams1 = 0;
  BatteryAlarams2 = 0;
  BatteryAlarams3 = 0;

  byte errorbit = 0x00000000;
  ///------------------------------------------------------
  if (ErrordataMaster1[7] == 1)  {
    errorbit0 = errorbit0 & 0b11011111;
    errorbit0 = errorbit0 | 0b00010000; /// Bit 0: Under-voltage
    //   Serial.println("Undervoltage");
  }
  ///------------------------------------------------------
  if (ErrordataMaster1[6] == 1) {
    errorbit0 = errorbit0 & 0b11110111;
    errorbit0 = errorbit0 | 0b00000100; /// Over-voltage
    //  Serial.println("Over Voltage");
  }
  ///------------------------------------------------------
  if (ErrordataMaster1[5] == 1) {
    errorbit1 = errorbit1 & 0b01111111;
    errorbit1 = errorbit1 | 0b01000000; /// Discharge Over-current
    //  Serial.println("Discharge Over-current");
  }
  ///------------------------------------------------------
  if (ErrordataMaster1[4] == 1) {
    errorbit2 = errorbit2 & 0b11111101;
    errorbit2 = errorbit2 | 0b00000001; /// Charge Over-current
    //  Serial.println("Charge Over-current");
  }
  ///------------------------------------------------------
  if (ErrordataMaster1[3] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000;
    //  Serial.println("BMS Internal- Cell Module Overheat");
  }
  ///------------------------------------------------------
  if ((ErrordataMaster1[1] == 1) || (MasterCANError == 1)) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// No Cell Communication
    //   Serial.println("BMS Internal- No Cell Comm");
  }
  ///------------------------------------------------------
  if (ErrordataMaster2[0] == 1)  {
    errorbit1 = errorbit1 & 0b11111101;
    errorbit1 = errorbit1 | 0b00000001; /// Low Temperature
    //  Serial.println("Cell Underheat");
    if (Reset_LowTemp_Current == 1)
    {
      errorbit1 = errorbit1 & 0b11011111;
      errorbit1 = errorbit1 | 0b00010000; /// battery temperature is lower than the battery
    }
    if ((BatterCurrt * 0.1) > 3) {
      errorbit1 = errorbit1 & 0b11011111;
      errorbit1 = errorbit1 | 0b00010000; /// charging while Error
      //   Serial.println("charging Module underheat");
      Reset_LowTemp_Current = 1;
    }
  }  else
  {
    Reset_LowTemp_Current = 0;
  }
  ///------------------------------------------------------
  if (ErrordataMaster2[4] == 1)  {
    errorbit0 = errorbit0 & 0b01111111;
    errorbit0 = errorbit0 | 0b01000000; /// Bit 1: Low Temperature
    //  Serial.println("Cell Over-Heat");
    if (Reset_HighTemp_Current == 1)    {
      errorbit1 = errorbit1 & 0b11110111;
      errorbit1 = errorbit1 | 0b00000100; /// Bit 2: battery temperature is lower than the battery accepts for charging
    }
    if ((BatterCurrt * 0.1) > 3) {
      errorbit1 = errorbit1 & 0b11110111;
      errorbit1 = errorbit1 | 0b00000100; /// Bit 2: battery temperature is lower than the battery accepts for charging
      //    Serial.println("charging Module Over-Heat");
      Reset_HighTemp_Current = 1;
    }
  }
  else  {
    Reset_HighTemp_Current = 0;
  }
  ///------------------------------------------------------
  if (ErrordataMaster2[3] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 6: No current Sensor No Cell Communication -- imbalance between cells
    //  Serial.println("BMS Internal,LEM");
  }
  ///------------------------------------------------------
  if (ErrordataMaster2[2] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 0: PackUnder-voltage
    // Serial.println("BMS- internal Pack Undervoltage");
  }
  ///------------------------------------------------------
  if (ErrordataMaster2[1] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 1: PackUnder-voltage
    // Serial.println("BMS Internal Pack Over-Voltage");
  }
  ///------------------------------------------------------
  if (ErrordataMaster6[7] == 1) {
    errorbit3 = errorbit3 & 0b11111101; /// Good
    errorbit3 = errorbit3 | 0b00000001; /// Bit 1: PackUnder-voltage
    //  Serial.println("Cell Voltage deviation");
  }
  ///------------------------------------------------------
  if (ErrordataMaster6[22] == 11) { ///?
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("BMS Pack Volt Deviation");
  }
  ///------------------------------------------------------
  /////////////////////////
  if ( errorbit0 != 0b10101010 ) BatteryAlarams0 = errorbit0;
  if ( errorbit1 != 0b10101010 ) BatteryAlarams1 = errorbit1;
  if ( errorbit2 != 0b10101010 ) BatteryAlarams2 = errorbit2;
  if ( errorbit3 != 0b10101010 ) BatteryAlarams3 = errorbit3;
  ////////////////////////
  ///---------------------------------------------
  Warnings_Master = data189[1];
  decToBinary(Warnings_Master);
  for (int j = 7; j >= 0; j--)  {
    WarndataMaster[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    WarndataMaster[i] = WarndataMaster[i];
  }
  //----------------------------------------------
  warnbit4 = 0b10101010;
  warnbit5 = 0b10101010;
  warnbit6 = 0b10101010;
  warnbit7 = 0b10101010;
  BatteryWanings = 0;
  ///------------------------------------------------------
  if (WarndataMaster[7] == 1) {
    warnbit4 = warnbit4 & 0b11011111;;
    warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //   Serial.println("Wlow Cell voltage");
  }
  if (WarndataMaster[6] == 1) {
    warnbit5 = warnbit5 & 0b01111111;;
    warnbit5 = warnbit5 | 0b01000000; /// Bit 1: High current – discharge current (negative current) exceeds the current warning setting.
    //  Serial.println("Whigh dichar current");
  }
  if (WarndataMaster[5] == 1) {
    warnbit6 = warnbit6 & 0b01111111;;
    warnbit6 = warnbit6 | 0b01000000; /// Bit 2: High temperature – cell module temperature exceeds warning temperature setting
    //  Serial.println("WBMS high Cell Module temp");
  }
  //-------------------------------------
  if (WarndataMaster4[7] == 1) {
    warnbit4 = warnbit4 & 0b11011111;;
    warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WlCell under-volt");
  }
  if (WarndataMaster4[6] == 1) {
    warnbit4 = warnbit4 & 0b11110111;;
    warnbit4 = warnbit4 | 0b00000100; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    // Serial.println("W Cell Over-Volt");
  }
  /* if (WarndataMaster[7] == 1) {
     warnbit4 = warnbit4 & 0b11011111;;
     warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
     Serial.println("WPack Under-Volt");
    }*/
  /* if (WarndataMaster[7] == 1) {
     warnbit4 = warnbit4 & 0b11011111;;
     warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
     Serial.println("WPack Over-Volt");
    }*/
  if (WarndataMaster4[5] == 1) {
    warnbit5 = warnbit5 & 0b01111111;;
    warnbit5 = warnbit5 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    // Serial.println("WDisch Over-Curr");
  }
  if (WarndataMaster4[4] == 1) {
    warnbit6 = warnbit6 & 0b11111101;;
    warnbit6 = warnbit6 | 0b00000001; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("W Charge Over curr");
  }
  if (WarndataMaster4[2] == 1) {
    warnbit4 = warnbit4 & 0b01111111;;
    warnbit4 = warnbit4 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WCell Over-heat");
    if (Reset_WHighTemp_Current == 1) {
      warnbit5 = warnbit5 & 0b11110111;;
      warnbit5 = warnbit5 | 0b00000100; /// Bit 2: High temperature – charge
    }
    if ((BatterCurrt * 0.1) > 3) {
      warnbit5 = warnbit5 & 0b11110111;;
      warnbit5 = warnbit5 | 0b00000100; /// Bit 2: High temperature – charge
      //   Serial.println("Wcharge high temp");
      Reset_WHighTemp_Current = 1;
    }
  }
  else  {
    Reset_WHighTemp_Current = 0;
  }
  if (WarndataMaster4[0] == 1) {
    warnbit5 = warnbit5 & 0b11111101;;
    warnbit5 = warnbit5 | 0b00000001; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("W Cell under-heat");
    if (Reset_WLowTemp_Current == 1) {
      warnbit5 = warnbit5 & 0b11011111;;
      warnbit5 = warnbit5 | 0b00010000; /// Bit 2: High temperature – charge
    }
    if ((BatterCurrt * 0.1) > 3) {
      warnbit5 = warnbit5 & 0b11011111;;
      warnbit5 = warnbit5 | 0b00010000; /// Bit 2: High temperature – charge
      //    Serial.println("Wcharge Low temp");
      Reset_WLowTemp_Current = 1;
    }
  }
  else {
    Reset_WLowTemp_Current = 0;
  }
  if (WarndataMaster5[2] == 1) {
    warnbit6 = warnbit6 & 0b01111111;;
    warnbit6 = warnbit6 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WBMS Cell Comm Loss");
  }
  if (WarndataMaster5[7] == 1) {
    warnbit7 = warnbit7 & 0b11111101;;
    warnbit7 = warnbit7 | 0b00000001; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WLow Cell Devi");
  }
  /////////////////////////
  if ( warnbit4 != 0b10101010 ) BatteryWanings = warnbit4;
  if ( warnbit5 != 0b10101010 ) BatteryWanings = warnbit5;
  if ( warnbit6 != 0b10101010 ) BatteryWanings = warnbit6;
  if ( warnbit7 != 0b10101010 ) BatteryWanings = warnbit7;
  ////////////////////////
}
// ------------------------------------------------------
void RelayControl() {
  CANESP.beginPacket(0x35A);
  CANESP.write(errorbit0);
  CANESP.write(errorbit1);
  CANESP.write(errorbit2);
  CANESP.write(errorbit3);
  CANESP.write(warnbit4);
  CANESP.write(warnbit5);
  CANESP.write(warnbit6);
  CANESP.write(warnbit7);
  CANESP.endPacket();
}
//  --------------------------------------------------------------
void onReceive(int packet)  {

  digitalWrite(LED_G, ON);
  digitalWrite(LED_R, OFF);

  for (int i = 0; i < 8; i++)  {
    if (CANESP.available())    {
      if (CANESP.packetId() == 0x355)       {
        data355[i] = CANESP.read();
        MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x356)  {
        data356[i] = CANESP.read();
        MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x351)  {
        data351[i] = CANESP.read();
        MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x189)  {
        data189[i] = CANESP.read();
        MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x305)  {
        data305[i] = CANESP.read();
      }
      else if (CANESP.packetId() == 0x306)  {
        data306[i] = CANESP.read();
      }
      else {
      }
    }
    else   {
    }
  }
}
///////////////////////////
void SerialDebug() {
  Serial.print("BattCharVolt ");  Serial.print(BattCharVolt); Serial.print(" ");
  Serial.print("ChargCurrLim ");  Serial.print(ChargCurrLim); Serial.print(" ");
  Serial.print("DischCurrLim ");  Serial.print(DischCurrLim); Serial.print(" ");
  Serial.print("DischVolt ");  Serial.print(DischVolt); Serial.println(" ");

  Serial.print("SOC ");  Serial.print(SOC); Serial.print(" ");
  Serial.print("SOH ");  Serial.print(SOH); Serial.print(" ");
  Serial.print("HiSOC ");  Serial.print(HiSOC); Serial.println(" ");

  Serial.print("BattVoltage ");  Serial.print(BattVoltage); Serial.print(" ");
  Serial.print("BatterCurrt ");  Serial.print(BatterCurrt * scalingfact1 ); Serial.print(" ");
  Serial.print("BattTempe ");  Serial.print(BattTempe); Serial.println(" ");

  Serial.print("SMABattVolt ");  Serial.print(SMABattVolt); Serial.print(" ");
  Serial.print("SMABattCurrent ");  Serial.print(SMABattCurrent * scalingfact1); Serial.print(" ");
  Serial.print("SMABattTemp ");  Serial.print(SMABattTemp); Serial.print(" ");
  Serial.print("SMASOC ");  Serial.print(SMASOC); Serial.print(" ");
  Serial.print("SMAErrorMEss ");  Serial.print(SMAErrorMEss); Serial.println(" ");
}
///////////////
void CANComm() { /// 1 Sec Intervel
  if (MasterCANbus > 2) {
    MasterCANbus = 0; MasterCANError = 0; ;
  } else {
    MasterCANError = 1;
  }
}
/////////
// function to convert decimal to binary
void decToBinary(int n) {
  for (int j = 0; j <= 8; j++) {
    binaryNum[j] = 0;
  }
  if (n != 0)  {
    // counter for binary array
    int i = 0;
    while (n > 0) {
      // storing remainder in binary array
      binaryNum[i] = n % 2;
      n = n / 2;
      i++;
    }
  }
  else  {
    for (int k = 0; k <= 8; k++)    {
      binaryNum[k] = 0;
    }
  }
}
void Write_SDCard() { /// 1 Sec Intervel
  /*
    SD Card, Save Data
  */
  if (ActiveSDcard) {
    myFile = SD.open(Filename, "a"); //append to file
    Serial.println(Filename);
    if (myFile)
    {
      CounterTime++;
      String Headline1 = String(CounterTime) + semi + String(BattCharVolt, 3) + semi + String(ChargCurrLim, 3) + semi + String(DischCurrLim, 3) + semi + String(DischVolt, 3) + semi + String(SOC, 3) + semi + String(SOH, 3) +
                         semi + String(HiSOC)  + semi + String(BattVoltage, 3)  + semi + String(BatterCurrt * 0.1, 3)  + semi +  String(BattTempe, 3)
                         + semi +   String(BatteryAlarams0) + semi +   String(BatteryAlarams1) + semi +   String(BatteryAlarams2) + semi +   String(BatteryAlarams3)
                         + semi + String(BatteryWanings) + semi + String(SMABattVolt, 3) + semi + String(SMABattCurrent * 0.1, 3) + semi +  String(SMABattTemp, 3) + semi + String(SMASOC, 3) + semi + String(SMAChar_OpeStae) +
                         semi + String(SMAErrorMEss)  + semi + String(SMABattCharPoint, 3);
      /*+ semi +   String(Batt1CellMax, 3) + semi + String(Batt2CellMax, 3)  +
        semi + String(Batt1CellMin, 3)  +  semi + String(Batt2CellMin, 3)  + semi +  String(Batt1RelaySta)  + semi + String(Batt2RelaySta) + semi + String(batt1Err) + semi + String(batt2Err)  + semi + String(SSMAVolt, 3) +
        semi + String(SSMACurr, 3)   + semi + String(SMAStatus) + semi + String(SSMATemp) + semi + String(Batter1NTC1) + semi + String(Batter1NTC2) + semi + String(Batter1NTC3) + semi + String(Batter2NTC1) +
        semi + String(Batter2NTC2) + semi + String(Batter2NTC3) + semi + String(MSB1 + LSB1); //+ semi + String(Errbites0) + semi + String(Errbites1) + semi + String(Errbites2) + semi + String(Errbites3);
      */
      myFile.println(Headline1);
      myFile.close();
    }
    else
    {
      Serial.println("error opening BatteryX.csv to write");
      digitalWrite(LED_B, ON);
      digitalWrite(LED_G, OFF);
      digitalWrite(LED_R, OFF);
    }
    ////////////////////////////////////////////////////////
  }
}
////////////////////////////////////////////////////////
